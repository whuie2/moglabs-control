pub use serial2::{
    CharSize,
    Parity,
    StopBits,
};

pub use crate::{
    mogdriver::{
        MOGDriver,
        SerialSettings,
        TableEntry,
        ChannelMode,
        ClockSource,
        ModulationMode,
        OutputMode,
        TableEntryFlags,
        TableTriggerEdge,
        MOGError,
        MOGResult,
    },
};
