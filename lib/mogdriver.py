"""
Class for interacting with MOGLabs devices. Based off the official source from
MOGLabs with modifications for running on non-Windows machines. See Appendix C
of the manual for more details on individual commands.

Requires pyserial.
"""
from __future__ import annotations
import os
import time
import socket
import serial
import select
from struct import unpack
from collections import OrderedDict
import six
import pathlib
import copy
import toml
# import plotdefs as pd
CRLF = b"\r\n"

def _fresh_filename(path: pathlib.Path, overwrite: bool=False) -> pathlib.Path:
    if overwrite:
        return path
    _path = path
    while _path.is_file():
        print(f"WARNING: found existing file {_path}")
        _path = _path.parent.joinpath(_path.stem + "_" + _path.suffix)
        print(f"Write instead to {_path}")
    return _path

class MOGEvent:
    """
    Holds the time and state data of an event.

    Fields
    ------
    frequency : float or None
        RF drive frequency in MHz.
    power : float or None
        RF drive power in dBm.
    phase : float or None
        RF drive phase in degrees.
    time : float
        Time of the event in seconds.
    """

    def __init__(self, frequency: float=None, power: float=None,
            phase: float=None, time: float=None):
        """
        Constructor. All arguments are optional, but the MOGEvent object must
        have a non-`None` value for its `time` field by the time it is added to
        a MOGTable.

        Parameters
        ----------
        frequency : float (optional)
            RF drive frequency in MHz.
        power : float (optional)
            RF drive power in dBm.
        phase : float (optional)
            RF drive phase in degrees.
        time : float (optional)
            Time of the event in seconds.
        """
        self.frequency = frequency
        self.power = power
        self.phase = phase
        self.time = 5e-6 if time == 0.0 else time

    def __matmul__(self, time: float):
        """
        Set self.time and return self, e.g.:
        ```
        E = MOGEvent(...) @ time
        ```
        """
        assert isinstance(time, (float, int))
        self.time = 5e-6 if time == 0.0 else time
        return self

    def gen_args(self, frequency0: float, power0: float, phase0: float,
            time0: float) -> tuple:
        """
        Generate the proper arguments to pass to the backend function.

        Parameters
        ----------
        frequency0, power0, phase0, time0 : float
            Previous state of the channel on which the table will be run.
        """
        return (
            self.frequency if self.frequency is not None else frequency0,
            self.power if self.power is not None else power0,
            self.phase if self.phase is not None else phase0,
            self.time - time0,
        )

    def to_primitives(self) -> dict[str, ...]:
        """
        Generate a dictionary containing all event data.
        """
        return {
            k: float(getattr(self, k))
            for k in ["frequency", "power", "phase", "time"]
            if getattr(self, k)
        }

    @staticmethod
    def from_primitives(alldata: dict[str, ...]):
        """
        Construct a MOGEvent from a dictionary containing event data following
        the format output by MOGEvent.to_primitives.
        """
        assert "time" in alldata.keys(), \
                "MOGEvent.from_primitives: missing required key 'time'"
        return MOGEvent(**alldata)

    def __str__(self):
        return f"MOGEvent(\n" \
            + f"  frequency = {self.frequency},\n" \
            + f"      power = {self.power},\n" \
            + f"      phase = {self.phase}\n" \
            + f") @ t = {self.time}"

class MOGTable:
    """
    List-like of Event objects.

    Fields
    ------
    events: list[Event]
    color: str
    stack_idx: int
    """

    def __init__(self, events: list[MOGEvent]=None, color: str=None, 
            stack_idx: float=None):
        """
        Default constructor.

        Parameters
        ----------
        events: list[MOGEvent] (optional)
        color: str (optional)
        stack_idx: int (optional)
        """
        self.events = list() if events is None else events
        self.color = color
        self.stack_idx = stack_idx
        self.is_sorted = False

    def with_color(self, color: str):
        self.color = color
        return self

    def with_stack_idx(self, stack_idx: int):
        assert isinstance(stack_idx, int), "Stack idx must be an integer"
        self.stack_idx = stack_idx
        return self

    def __lshift__(self, event: MOGEvent):
        """
        Append an Event and return self.
        """
        assert isinstance(event, MOGEvent)
        assert event.time is not None
        self.events.append(event)
        return self

    def __getitem__(self, idx: int):
        return self.events[idx]

    def __add__(self, other: MOGTable):
        """
        Concatenate two MOGTables.
        """
        assert isinstance(other, MOGTable)
        return MOGTable(self.events + other.events)

    def sort(self):
        self.events = sorted(self.events, key=lambda e: e.time)
        self.is_sorted = True
        return self

    def __iter__(self):
        if not self.is_sorted:
            self.sort()
        return iter(self.events)

    def __len__(self):
        return len(self.events)

    def __str__(self):
        return f"MOGTable([\n" \
            + ",\n".join(
                "\n".join("  " + s for s in str(e).split("\n"))
                for e in self.events
            ) \
            + "\n])"

    def pop(self, idx: int=-1) -> MOGEvent:
        """
        Pop the MOGEvent at index `idx` from self.
        """
        return self.events.pop(idx)

    def insert(self, idx: int, event: MOGEvent):
        """
        Insert a MOGEvent at index `idx` and return self.
        """
        assert isinstance(event, MOGEvent)
        assert event.time is not None
        self.events.insert(idx, event)
        return self

    def append(self, event: MOGEvent):
        """
        Append a MOGEvent and return self.
        """
        assert isinstance(event, MOGEvent)
        assert event.time is not None
        self.events.append(event)
        return self

    def min_time(self) -> float:
        """
        Find the time of the earliest event in self.events.
        """
        return min(map(lambda e: e.time, self.events)) \
                if len(self.events) > 0 else 0.0

    def max_time(self) -> float:
        """
        Find the time of the latest event in self.events.
        """
        return max(map(lambda e: e.time, self.events)) \
                if len(self.events) > 0 else 0.0

    def when(self) -> (float, float):
        """
        Return (self.min_time(), self.max_time()).
        """
        return (self.min_time(), self.max_time())

    def _get_states(self) -> list[tuple]:
        return [
            (e.time, e.frequency, e.power, e.phase)
            for e in self.events
        ]

    def to_primitives(self) -> dict[str, ...]:
        """
        Generate a dictionary containing all sequence data as only primitive
        types.
        """
        return {
            "events": [event.to_primitives() for event in self.events],
            "color": self.color,
            "stack_idx": self.stack_idx
        }

    @staticmethod
    def from_primitives(alldata: dict[str, ...]):
        """
        Construct a MOGTable object from a dict containing data following the
        format output by MOGTable.to_primitives.
        """
        assert "events" in alldata.keys(), \
                "MOGTable.from_primitives: missing required key 'events'"
        return MOGTable(
            [MOGEvent.from_primitives(event_dict)
                for event_dict in alldata["events"]],
            alldata.get("color"),
            alldata.get("stack_idx")
        )

class MOGSuperTable:
    """
    Table of labeled MOGTables. Mostly for visualization purposes.
    Visualization implementation is still in progress.

    Fields
    ------
    outdir : pathlib.Path
    name : str
    tables : dict[str, Sequence]
    """

    def __init__(self, outdir: pathlib.Path, name: str,
            tables: dict[str, MOGTable]=None):
        """
        Constructor.

        Parameters
        ----------
        outdir : pathlib.Path
        name : str
        tables : dict[str, MOGTable] (optional)
        """
        self.outdir = outdir
        self.name = name
        self.tables = dict() if tables is None else tables

    def __getitem__(self, key: str) -> MOGTable:
        assert isinstance(key, str)
        return self.tables[key]

    def __setitem__(self, key: str, tab: MOGTable):
        assert isinstance(key, str)
        assert isinstance(tab, MOGTable)
        self.tables[key] = tab

    def get(self, key, default) -> MOGTable:
        return self.tables.get(key, default)

    def keys(self):
        return self.tables.keys()

    def values(self):
        return self.tables.values()

    def items(self):
        return self.tables.items()

    def update(self, other):
        self.tables.update(other)
        return self

    def __add__(self, other):
        assert isinstance(other, MOGSuperTable)
        tables = copy.deepcopy(self.tables)
        return MOGSuperTable(self.outdir, self.name, tables).update(other)

    def __iadd__(self, other):
        return self.update(other)

    def by_times(self) -> list[MOGTable]:
        """
        Return MOGTables in a list ordered by earliest MOGEvent.
        """
        return sorted(
            self.tables.values(),
            key=lambda e: e.min_time()
        )

    def to_mogtable(self) -> MOGTable:
        """
        Condense to a single MOGTable to pass to the driver.
        """
        tab = MOGTable()
        for T in self.by_times():
            tab = tab + T
        return tab

    def to_primitives(self) -> dict[str, dict]:
        return {
            label: tab.to_primitives() for label, tab in self.tables.items()
        }

    @staticmethod
    def from_primitives(outdir: pathlib.Path, name: str,
            alldata: dict[str, dict]):
        return MOGSuperTable(outdir, name, {
            label: MOGTable.from_primitives(tab_dict)
            for label, tab_dict in alldata.items()
        })

    def save(self, target: pathlib.Path=None, overwrite: bool=False,
            printflag: bool=True):
        T = self.outdir.joinpath(self.name) if target is None else target
        T = T.parent.joinpath(T.name + ".toml") if T.suffix != ".toml" else T
        if printflag: print(f"[mogdriver] Saving table data to '{T}':")
        if not T.parent.is_dir():
            if printflag: print(f"[mogdriver] mkdir {T.parent}")
            T.parent.mkdir(parents=True, exist_ok=True)

        table_file = _fresh_filename(T, overwrite)
        if printflag: print(f"[mogdriver] save table '{T.stem}'")
        with table_file.open('w') as outfile:
            toml.dump(self.to_primitives(), outfile)
        return self

    @staticmethod
    def load(target: pathlib.Path, outdir: pathlib.Path=None, name: str=None,
            printflag: bool=True):
        outdir = target.parent if outdir is None else outdir
        name = target.stem if name is None else name
        if printflag: print(f"[mogdriver] Loading data from target '{target}':")
        if not target.is_dir():
            raise Exception(f"Target '{target}' does not exist")

        table_file = target
        if printflag: print("[mogdriver] load table")
        if not table_file.is_file():
            raise Exception(f"Table file '{table_file}' does not exist")
        tab_dict = toml.load(table_file)
        return MOGSuperTable.from_primitives(outdir, name, tab_dict)

class MOGDriver:
    """
    Class to drive communication with a MOGLabs device. All methods that do not
    have an explicit return value return `self`.
    """

    def __init__(self, addr: str, port: int):
        """
        Constructor. Initializes the object, but does not automatically connect.

        Parameters
        ----------
        addr : str
            Address of the device. Must be 'COM' or a '/dev' path for a USB
            connection, otherwise assumed to be an IP address.
        port : int
            Port number for the address. For USB connections, this is the number
            that goes after 'COM' or '/dev/ttyACM'.
        """
        self.dev = None
        if addr == "COM":
            self.connection = f"{addr}{port}"
            self.is_usb = True
        elif addr.startswith("/dev"):
            if os.geteuid() != 0:
                raise Exception("Requires root privileges")
            self.connection = f"{addr}{port}"
            self.is_usb = True
        else:
            self.connection = f"{addr}:{port}"
            self.is_usb = False
        self.is_connected = False

    def connect(self, timeout: float=1.0, check: bool=False,
            timeout_retry: int=0):
        """
        Connect to the stored address. Disconnects and reconnects if already
        connected.

        Parameters
        ----------
        timeout : float (optional)
            Wait `timeout` seconds for a response from the device.
        check : bool (optional)
            Perform a test query after forming a connection.
        timeout_retry : int (optional)
            Retry a command a number of times if a timeout is encountered.
        """
        self.timeout_retry = timeout_retry
        if self.is_connected:
            self.close()

        if self.is_usb:
            try:
                self.dev = serial.Serial(
                    self.connection, baudrate=115200, bytesize=8, parity="N",
                    stopbits=1, timeout=timeout, writeTimeout=0)
            except serial.SerialException as err:
                raise RuntimeError(err.args[0].split(":", 1)[0])
        else:
            self.dev = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.dev.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.dev.settimeout(timeout)
            addr, port = self.connection.split(":")
            self.dev.connect((addr, int(port)))

        if check:
            try:
                self.info = self.ask("info")
            except Exception as err:
                raise RuntimeError("Device is unresponsive")

        self.is_connected = True
        return self

    def close(self):
        """
        Close any active connection. Can be reconnected later.
        """
        if self.is_connected:
            self.dev.close()
            self.dev = None
            self.is_connected = False
        return self

    def disconnect(self):
        """
        Alias for `self.close`.
        """
        return self.close()

    def _check(self):
        assert self.is_connected, "Device is unresponsive"

    def _check_ch(self, *ch: int):
        assert all(c in range(1, 5) for c in ch), "Invalid channel"

    def get_timeout(self) -> float:
        """
        Get the current connection timeout in seconds.
        """
        return self.dev.timeout if self.is_usb else self.dev.gettimeout()

    def set_timeout(self, val: float):
        """
        Change the timeout to the given value in seconds.
        """
        self._check()
        if self.is_usb:
            self.dev.timeout = val
        else:
            self.dev.settimeout(val)
        return self

    def send_raw(self, cmd: str):
        """
        Send `cmd` to the device unmodified.
        """
        self._check()
        if self.is_usb:
            self.dev.write(cmd)
        else:
            self.dev.send(cmd)
        return self

    def send(self, cmd: str):
        """
        Send `cmd` to the device, appending newline if not already present.
        """
        _cmd = cmd
        if hasattr(_cmd, "encode"):
            _cmd = _cmd.encode()
        if not _cmd.endswith(CRLF):
            _cmd += CRLF
        self.send_raw(_cmd)
        return self

    def receive_raw(self, size: int) -> bytes:
        """
        Receive exactly `size` bytes from the device.
        """
        self._check()
        parts = list()
        t_timeout = time.time() + self.get_timeout()
        while size > 0:
            if self.is_usb:
                chunk = self.dev.read(min(size, 0x2000))
            else:
                chunk = self.dev.recv(min(size, 0x2000))
            if time.time() > t_timeout:
                raise DeviceError("Reached timeout")
            parts.append(chunk)
            size -= len(chunk)
        return b''.join(parts)

    def receive(self, bufsize: int=256) -> str:
        """
        Receive one line of data from the device, returned as a str.
        """
        self._check()
        if self.is_usb:
            data = self.dev.readline(bufsize)
            if len(data):
                while self.has_data(timeout=0):
                    segment = self.dev.readline(bufsize)
                    if not len(segment):
                        break
                    data += segment
            if not len(data):
                raise RuntimeError("Reached timeout")
        else:
            data = b''
            while True:
                data += self.dev.recv(bufsize)
                timeout = 0 if data.endswith(CRLF) else 0.1
                if not self.has_data(timeout):
                    break
        return data.decode()

    def has_data(self, timeout: float=0.0) -> bool:
        """
        Returns True if there is data waiting in the queue, otherwise False.
        """
        self._check()
        if self.is_usb:
            try:
                if self.dev.inWaiting():
                    return True
                if timeout == 0:
                    return False
                time.sleep(timeout)
                return self.dev.inWaiting() > 0
            except serial.SerialException:
                return False
        else:
            sel = select.select([self.dev], [], [], timeout)
            return len(sel[0]) > 0

    def flush(self, timeout: float=0.0, bufsize: int=256) -> str:
        """
        Flush and return all data from the device's output queue as a str.
        """
        self._check()
        data = str()
        while self.has_data(timeout):
            chunk = self.receive(bufsize)
            if isinstance(chunk, six.binary_type) \
                    and not isinstance(data, six.binary_type):
                data = data.encode()
            data += chunk
        return data

    def ask(self, cmd: str) -> str:
        """
        Send followed by receive, returning the response as a str.
        """
        self._check()
        self.flush()
        self.send(cmd)
        response = self.receive().strip()
        if response.startswith("ERR:"):
            raise RuntimeError(response[4:].strip())
        return response

    def ask_dict(self, cmd: str) -> dict:
        """
        Send a request that return a dictionary response, with keys and values
        as strs.
        """
        self._check()
        response = self.ask(cmd)
        if response.startswith("OK"):
            response = response[3:].strip()
        if ":" not in response:
            raise RuntimeError(f"Response to {cmd} is not a dictionary")
        splitchar = "," if "," in response else "\n"
        vals = OrderedDict()
        for entry in response.split(splitchar):
            key, val = entry.split(":")
            vals[key.strip()] = val.strip()
        return vals

    def ask_bin(self, cmd: str) -> bytes:
        """
        Send a request that returns a binary response.
        """
        self._check()
        self.send(cmd)
        head = self.receive_raw(4)
        if head == b"ERR:":
            raise RuntimeError(self.receive().strip())
        datalen = unpack("<L", head)[0]
        data = self.receive_raw(datalen)
        if len(data) != datalen:
            raise RuntimeError("Binary response block has incorrect length")
        return data

    def cmd(self, cmd: str) -> str:
        """
        Send the specified command and check that the response is OK. Returns
        the response as a str.
        """
        response = self.ask(cmd)
        if not response.startswith("ERR"):
            return response
        elif "Reached timeout" in response and self.timeout_retry:
            print(f"[mogdriver] received timeout; retrying {self.timeout_retry} time(s)...")
            for _ in range(self.timeout_retry):
                response = self.ask(cmd)
                print(response)
                if not response.startswith("ERR"):
                    return response
            raise RuntimeError(response)
        else:
            raise RuntimeError(response)

    def reboot(self):
        """
        Reboot the device and close the connection.
        """
        self.send("REBOOT")
        self.close()
        return self

    def get_info(self) -> str:
        """
        Report information about the unit.
        """
        return self.ask("INFO")

    def get_version(self) -> str:
        """
        Report versions of firmware currently running on the device.
        """
        return self.ask("VERSION")

    def get_temp(self) -> str:
        """
        Report measured temperatures and fan speeds.
        """
        return self.ask("TEMP")

    def get_status(self, ch: int) -> str:
        """
        Report the current operational status of the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"STATUS,{ch}")

    def get_mode(self, ch: int) -> str:
        """
        Get the current operational mode of the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"MODE,{ch}")

    def set_mode(self, ch: int, ty: str):
        """
        Set the operational mode of the given channel. `ty` must be one of
        {'NSB', 'NSA', or 'TSB'} (see manual for details). Automatically
        switches off the output of the specified channel.
        """
        self._check_ch(ch)
        assert ty.upper() in {"NSB", "NSA", "TSB"}, "Invalid mode type"
        self.cmd(f"MODE,{ch},{ty.upper()}")
        return self

    def set_output(self, ch: int, onoff: bool, ty: str="ALL"):
        """
        Enable or disable the RF output of the specified channel. The signal and
        amplifiers can be individually controlled with `ty`, which must be one
        of {'SIG', 'POW', 'ALL'} (see manual for details).
        """
        self._check_ch(ch)
        assert ty.upper() in {"ALL", "SIG", "POW"}, "Invalid mode type"
        self.cmd(("ON" if onoff else "OFF") + f",{ch},{ty.upper()}")
        return self

    def sleep(self, dt: float):
        """
        Pause the microcontroller operation for `dt` milliseconds, during which
        the microcontroller will be unresponsive.
        """
        self.cmd(f"SLEEP,{dt}")
        return self

    def get_frequency(self, ch: int) -> str:
        """
        Get the current frequency (in MHz) of the output from the specified
        channel.
        """
        self._check_ch(ch)
        return self.cmd(f"FREQ,{ch}")

    def set_frequency(self, ch: int, val: float):
        """
        Set the frequency (in MHz) of the output from the specified channel, as
        permitted by the DDS's 32-bit frequency resolution.
        """
        self._check_ch(ch)
        assert abs(val) >= 10 and abs(val) <= 200, \
                "Invalid frequency, must be in the range [10, 200]"
        self.cmd(f"FREQ,{ch},{val}")
        return self

    def get_power(self, ch: int) -> str:
        """
        Get the power (in dBm) of the output from the specified channel, as
        computed using factory calibration.
        """
        self._check_ch(ch)
        return self.cmd(f"POW,{ch}")

    def set_power(self, ch: int, val: float):
        """
        Set the power (in dBm) of the output from the specified channel, as
        computed using factory calibration. Power is limited to the value set by
        the `set_limit` command.
        """
        self._check_ch(ch)
        #assert val >= 0, "Invalid power"
        self.cmd(f"POW,{ch},{val}")
        return self

    def get_limit(self, ch: int) -> str:
        """
        Get the maximum RF power (in dBM) for the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"LIMIT,{ch}")

    def set_limit(self, ch: int, val: float):
        """
        Set the maximum RF power (in dBm) for the specified channel. If the
        limit is set below the current power level, the current power is reduced
        to the limit.
        """
        self._check_ch(ch)
        assert val >= 0, "Invalid power"
        self.cmd(f"LIMIT,{ch},{val}")
        return self

    def get_phase(self, ch: int) -> str:
        """
        Get the current phase offset (in degrees) of the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"PHASE,{ch}")

    def set_phase(self, ch: int, val: float):
        """
        Set the phase offset (in degrees) of the specified channel.
        """
        self._check_ch(ch)
        self.cmd(f"PHASE,{ch},{val}")
        return self

    def align_phase(self, *ch: int):
        """
        Simultaneously resets the phase accumulators of the specified channels.
        If no channels are specified, all channels are reset.
        """
        self._check_ch(*ch)
        _ch = (len(ch) > 0) * "," + ",".join(str(c) for c in ch)
        self.cmd(f"ALIGNPH{_ch}")
        return self

    def set_phase_reset(self, onoff: bool, *ch: int):
        """
        Enable or disable automatic phase alignment on the specified channels,
        which will be performed after any `set_freq` command is issued. If no
        channels are specified, apply to all channels.
        """
        self._check_ch(*ch)
        _ch = (len(ch) > 0) * "," + ",".join(str(c) for c in ch)
        _onoff = "ON" if onoff else "OFF"
        self.cmd(f"PHRESET,{_ONOFF}{_ch}")
        return self

    def get_modulation(self, ch: int) -> str:
        """
        Get the current modulation on the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"MOD,{ch}")

    def set_modulation(self, ch: int, ty: str, onoff: bool, gain: float):
        """
        Set the gain for a modulation type on the specified channel. `ty` must
        one of {'FREQ', 'AMPL', 'PHAS', 'NONE'}.
        """
        self._check_ch(ch)
        assert ty.upper() in {"FREQ", "AMPL", "PHAS", "NONE"}, \
                "Invalid modulation type"
        _onoff = "ON" if onoff else "OFF"
        self.cmd(f"MOD,{ch},{ty.upper()},{_ONOFF},{gain}")
        return self

    def set_modulation_dual(self, ch: int, ty1: str, ty2: str):
        """
        Set the primary and secondary modulation types. `ty1` and `ty2` must be
        one of {'FREQ', 'AMPL', 'PHAS', 'NONE'}.
        """
        self._check_ch(ch)
        assert all(
            ty.upper() in {"FREQ", "AMPL", "PHAS", "NONE"} for ty in {ty1, ty2}
        ), "Invalid modulation type"
        self.cmd(f"MOD,{ch},{ty1.upper()},{ty2.upper()}")
        return self

    def get_clock_source(self) -> str:
        """
        Get the current clock source.
        """
        return self.cmd(f"CLKSRC")

    def set_clock_source(self, source: str, freq: float=500.0):
        """
        Set the current clock source. `source` must be one of {'INT', 'EXT'}; if
        `source` is 'EXT', `freq` must be provided in MHz.
        """
        assert source.upper() in {"INT", "EXT"}, "Invalid clock source"
        assert freq >= 0
        self.cmd(f"CLKSRC,{source.upper()},{freq}")
        return self

    def get_table_status(self, ch: int) -> str:
        """
        Get the current execution status of the table on the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"TABLE,STATUS,{ch}")

    def table_clear(self, ch: int):
        """
        Stop execution of and clear all entries from the table on the specified
        channel.
        """
        self._check_ch(ch)
        self.cmd(f"TABLE,CLEAR,{ch}")
        return self

    def get_table_entry(self, ch: int, num: int) -> str:
        """
        Get the specified table entry on the specified channel.
        """
        self._check_ch(ch)
        assert num in range(1, 8192), "Invalid entry number"
        return self.cmd(f"TABLE,ENTRY,{ch},{num}")

    def set_table_entry(self, ch: int, num: int, freq: float, power: float,
            phase: float, dur: float, flag: str):
        """
        Set the specified table entry on the specified channel. `freq` is
        expected in MHz, `power` in dBm, `phase` in degrees, and `dur` in
        seconds. Note that durations are discretized to multiples of 5 us, so
        `dur` will be rounded down to the nearest multiple. `flag` must be one
        of {'SIG', 'POW', 'SIG,POW'}.
        """
        self._check_ch(ch)
        assert num in range(1, 8192), "Invalid entry number"
        assert flag.upper() in {"SIG", "POW", "SIG,POW"}, "Invalid entry flag"
        _dur = int(1e6 * dur / 5)
        while _dur >= 65535:
            self.cmd(f"TABLE,ENTRY,{ch},{num},\
                    {freq},{power},{phase},65535,{flag}")
            _dur -= 65535
        if _dur >= 0:
            self.cmd(f"TABLE,ENTRY,{ch},{num},\
                    {freq},{power},{phase},{_dur},{flag}")
        return self

    def get_table_entries(self, ch: int) -> str:
        """
        Get the number of the last entry of the table on the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"TABLE,ENTRIES,{ch}")

    def set_table_entries(self, ch: int, val: int):
        """
        Set the number of the last entry of the table on the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"TABLE,ENTRIES,{ch},{val}")

    def table_append(self, ch: int, freq: float, power: float, phase: float,
            dur: float, flag: str="SIG,POW"):
        """
        Append an entry to the end of the table of the specified channel and
        automatically increment the number of entries. See `set_table_entry` for
        constraints on parameters.
        """
        self._check_ch(ch)
        assert flag.upper() in {"SIG", "POW", "SIG,POW"}, "Invalid entry flag"
        _dur = int(1e6 * dur / 5)
        while _dur >= 65535:
            self.cmd(f"TABLE,APPEND,{ch},\
                    {freq},{power},{phase},65535,{flag}")
            _dur -= 65535
        if _dur == 0:
            self.cmd(f"TABLE,APPEND,{ch},{freq},{power},{phase},1,{flag}")
        elif _dur > 0:
            self.cmd(f"TABLE,APPEND,{ch},{freq},{power},{phase},{_dur},{flag}")
        return self

    def table_load(self, ch: int, table: MOGTable):
        """
        Load a series of table entries (MOGEvents) from a MOGTable object for
        the given channel.
        """
        _frequency = float(self.get_frequency(ch).split(" ")[0])
        _power = float(self.get_power(ch).split(" ")[0])
        _phase = float(self.get_phase(ch).split(" ")[0])
        _time = 0.0
        for e in table:
            self.table_append(
                ch, _frequency, _power, _phase, e.time - _time, "SIG,POW")
            _frequency = _frequency if e.frequency is None  else e.frequency
            _power = _power if e.power is None else e.power
            _phase = _phase if e.phase is None else e.phase
            _time = e.time
        self.table_append(ch, _frequency, _power, _phase, 5e-6, "SIG,POW")
        return self

    def table_copy(self, ch1: int, ch2: int):
        """
        Copy all table data from `ch1` to `ch2`.
        """
        self._check_ch(ch1, ch2)
        self.cmd(f"TABLE,APPEND,{ch1},{ch2}")
        return self

    def get_table_name(self, ch: int) -> str:
        """
        Get the current name of the table on the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"TABLE,NAME,{ch}")

    def set_table_name(self, ch: int, name: str):
        """
        Assign a character string to the table of the specified channel for
        identification purposes. Use quotation marks to preserve case.
        """
        self._check_ch(ch)
        self.cmd(f"TABLE,NAME,{ch},{name}")
        return self

    def table_arm(self, *ch: int):
        """
        Load the table(s) on the specified channel(s) for execution and begin
        watching for either a software or hardware trigger.
        """
        assert len(ch) > 0, "Must specify at least one channel"
        self._check_ch(*ch)
        _ch = ",".join(str(c) for c in ch)
        self.cmd(f"TABLE,ARM,{_ch}")
        return self

    def table_start(self, *ch: int):
        """
        Send a software trigger to begin execution of the table(s) on the
        specified channel(s).
        """
        assert len(ch) > 0, "Must specify at least one channel"
        self._check_ch(*ch)
        _ch = ",".join(str(c) for c in ch)
        self.cmd(f"TABLE,START,{_ch}")
        return self

    def table_trigsync(selc, *ch: int):
        raise NotImplemented

    def table_stop(self, *ch: int):
        """
        Send a kill signal to halt execution of the table(s) on the specified
        channel(s) at the end of the current step.
        """
        assert len(ch) > 0, "Must specify at least on channel"
        self._check_ch(*ch)
        _ch = ",".join(str(c) for c in ch)
        self.cmd(f"TABLE,STOP,{_ch}")
        return self

    def get_table_rearm(self, ch: int) -> str:
        """
        Get the current status of the table rearm switch for the specified
        channel.
        """
        self._check_ch(ch)
        return self.cmd(f"TABLE,REARM,{ch}")

    def set_table_rearm(self, ch: int, onoff: bool):
        """
        Enable or disable the automatic re-arming of the table on the specified
        channel upon its completion.
        """
        self._check_ch(ch)
        _onoff = "ON" if onoff else "OFF"
        self.cmd(f"TABLE,REARM,{ch},{_onoff}")
        return self

    def get_table_restart(self, ch: int) -> str:
        """
        Get the current status of the table restart switch for the specified
        channel.
        """
        self._check_ch(ch)
        return self.cmd(f"TABLE,RESTART,{ch}")

    def set_table_restart(self, ch: int, onoff: bool):
        """
        Enable or disable the automatic restart of the table on the specified
        channel upon its completion.
        """
        self._check_ch(ch)
        _onoff = "ON" if onoff else "OFF"
        self.cmd(f"TABLE,RESTART,{ch},{_onoff}")
        return self

    def get_table_edge(self, ch: int) -> str:
        """
        Get the current TTL trigger edge for the table on the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"TABLE,EDGE,{ch}")

    def set_table_edge(self, ch: int, ty: str):
        """
        Set the TTL trigger edge for the table on the specified channel as
        either 'RISING' or 'FALLING'.
        """
        self._check_ch(ch)
        assert ty.upper() in {"RISING", "FALLING"}, "Invalid trigger mode"
        self.cmd(f"TABLE,EDGE,{ch},{ty.upper()}")
        return self

    def get_pid_status(self, ch: int) -> str:
        """
        Get the current states of the PID controller and whether saturation has
        occurred. The saturation flag is set by this query.
        """
        self._check_ch(ch)
        return self.cmd(f"PID,STATUS,{ch}")

    def set_pid(self, ch: int, onoff: bool, ty: str="FREQ"):
        """
        Enable or disable the PID controller. Enabling requires specification of
        `ty`, which must be one of {'FREQ', 'AMPL', 'PHAS'}.
        """
        self._check_ch(ch)
        assert ty.upper() in {"FREQ", "AMPL", "PHAS"}, "Invalid mode"
        if onoff:
            self.cmd(f"PID,ENABLE,{ch},{ty}")
        else:
            self.cmd(f"PID,DISABLE,{h}")
        return self

    def get_pid_setpoint(self, ch: int) -> str:
        """
        Get the current value of the PID setpoint for the specified channel.
        """
        self._check_ch(ch)
        return self.cmd(f"PID,SETPOINT,{ch}")

    def set_pid_setpoint(self, ch: int, val: float):
        """
        Set the PID setpoint for the specified channel. `val` must be a voltage
        in the range [-1, +1].
        """
        self._check_ch(ch)
        assert val >= -1 and val <= +1, "Invalid setpoint value"
        self.cmd(f"PID,SETPOINT,{ch},{val}")
        return self

    def get_pid_gain(self, ch: int, name: str) -> str:
        """
        Get the current PID gain for the specified constant on the specified
        channel. `name` must be one of {'P', 'I', 'D'}.
        """
        self._check_ch(ch)
        assert name.upper() in {"P", "I", "D"}, "Invalid constant name"
        return self.cmd(f"PID,GAIN,{ch},{name.upper()}")

    def set_pid_gain(self, ch: int, name: str, val: float):
        """
        Set the PID gain for the specified constant on the specified channel.
        `name` must be one of {'P', 'I', 'D'} and `val` must be in the range
        [0, 1] unless `name` is 'D', in which case `val` must be in the range
        [-1, +1].
        """
        self._check_ch(ch)
        assert (name.upper() in {"P", "I"} and val >= 0 and val <= 1) \
                or (name.upper() == "D" and val >= -1 and val <= 1), \
                "Invalid value for specified constant"
        self.cmd(f"PID,GAIN,{ch},{name.upper()},{val}")
        return self

    def get_pid_error(self, ch: int) -> str:
        """
        Get the current value of the PID error signal.
        """
        self._check_ch(ch)
        return self.cmd(f"PID,ERROR,{ch}")

    def get_ethernet_info(self) -> str:
        """
        Report current ethernet settings.
        """
        return self.cmd(f"ETH,INFO")

    def get_ethernet_status(self) -> str:
        """
        Report current ethernet status.
        """
        return self.cmd(f"ETH,STATUS")

    def set_ethernet_server(self, onoff: bool):
        """
        Enable or disable the integrated web server for device control.
        """
        _onoff = "ON" if onoff else "OFF"
        self.cmd(f"ETH,WEB,{_onoff}")
        return self

    def set_ethernet_ip(self, ipaddr: str):
        """
        Set the IP address. The static address is used if DHCP fails or is
        disabled.
        """
        assert len(ipaddr.split(".")) == 4, "Invalid IP address"
        self.cmd(f"ETH,STATIC,{ipaddr}")
        return self

    def set_ethernet_mask(self, ipmask: str):
        """
        Set the IP mask.
        """
        assert len(ipmask.split(".")) == 4, "Invalid IP mask"
        self.cmd(f"ETH,MASK,{ipmask}")
        return self

    def set_ethernet_gateway(self, ipaddr: str):
        """
        Set the IP gateway.
        """
        assert len(ipaddr.split(".")) == 4, "Invalid IP gateway"
        self.cmd(f"ETH,GW,{ipaddr}")
        return self

    def set_ethernet_port(self, port: int):
        """
        Set the TCPIP port number for device communication.
        """
        self.cmd(f"ETH,PORT,{port}")
        return self

    def set_ethernet_dhcp(self, onoff: bool):
        """
        Enable or disable DHCP.
        """
        _onoff = "ON" if onoff else "OFF"
        self.cmd(f"ETH,DHCP,{_onoff}")
        return self

